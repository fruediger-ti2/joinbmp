/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 7 - Task 4                                                        *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     filebuffers.c                                    *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/joinbmp         *
 *                                                                            *
 ******************************************************************************/

#include <unistd.h>
#include <string.h>
#include "filebuffers.h"

inline void file_buffer_clear(file_buffer *buf, int fd)
{
    buf->fd   = fd;
    buf->pos  = 0;
    buf->size = 0;
}

inline ssize_t file_buffer_flush(file_buffer *buf)
{
    if (buf->size > 0)
    {
        ssize_t nwritten;
        if ((nwritten = write(buf->fd, &(buf->buffer[0]), buf->size)) > 0)
        {
            if (buf->size > nwritten)
            {
                buf->size = buf->pos = buf->size - nwritten;

                memmove(&(buf->buffer[0]), &(buf->buffer[nwritten]), buf->size);
            }
            else
            {
                buf->size = buf->pos = 0;
            }
        }

        return nwritten;
    }

    return 0;
}

inline ssize_t file_buffer_read(file_buffer *buf, void *data, size_t n)
{
    char *output = (char *)data;

    size_t k = 0;
    while (k < n)
    {
        if (buf->pos >= buf->size)
        {
            ssize_t nread;
            if ((nread = read(buf->fd, &(buf->buffer[0]), BUFFER_SIZE)) < 0)
                return nread;
            else if (nread == 0)
                return k;
            else
            {
                buf->pos  = 0;
                buf->size = nread;
            }
        }

        output[k++] = buf->buffer[buf->pos++];
    }

    return k;
}

inline ssize_t file_buffer_write(file_buffer *buf, const void *data, size_t n)
{
    const char *input = (const char *)data;

    size_t k = 0;
    while (k < n)
    {
        if (buf->pos >= BUFFER_SIZE)
        {
            ssize_t nwritten;
            if ((nwritten = file_buffer_flush(buf)) < 0)
                return nwritten;
            else if (nwritten == 0)
                return k;  
        }

        buf->buffer[buf->pos++] = input[k++];

        if (buf->size < buf->pos)
            buf->size = buf->pos;
    }

    return k;
}