/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 7 - Task 4                                                        *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     filebuffers.h                                    *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/joinbmp         *
 *                                                                            *
 ******************************************************************************/

#ifndef FILEBUFFERS_H__
#define FILEBUFFERS_H__

#include <stdlib.h>

#define BUFFER_SIZE (1024)

typedef struct
{
    int    fd;
    size_t pos;
    size_t size;
    char   buffer[BUFFER_SIZE];
} file_buffer;

void    file_buffer_clear(file_buffer *buf, int fd);
ssize_t file_buffer_flush(file_buffer *buf);
ssize_t file_buffer_read (file_buffer *buf, void       *data, size_t n);
ssize_t file_buffer_write(file_buffer *buf, const void *data, size_t n);

#endif