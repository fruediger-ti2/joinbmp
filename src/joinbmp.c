/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 7 - Task 4                                                        *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     joinbmp.c                                        *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/joinbmp         *
 *                                                                            *
 ******************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "filebuffers.h"
#include "joinbmp.h"

// we ensure packaging (via attributes and pragmas) in the following struct definitions
// since we're going to operate on file contents

// a default bitmap witdh 24 bits per pixel demands it's color representation in the BGR (blue-green-red) format
#pragma pack(1)
typedef struct __attribute__((packed))
{
    uint8_t b; // blue channel
    uint8_t g; // green channel
    uint8_t r; // red channel
} color_bgr_t;

#pragma pack(1)
typedef struct __attribute__((packed))
{
    uint16_t bfType;      // identifies BMP and DIB files
    uint32_t bfSize;      // the size of the BMP file in bytes
    uint16_t bfReserved1; // creating application reserved
    uint16_t bfReserved0; // creating application reserved
    uint32_t bfOffBits;   // the offset in bytes where the bitmap image data can be found
} BITMAPFILEHEADER;

#pragma pack(1)
typedef struct __attribute((packed))
{
    uint32_t biSize;          // the size of this header in bytes (40 bytes)
    int32_t  biWidth;         // the bitmap width in pixels
    int32_t  biHeight;        // the bitmap height in pixels
    uint16_t biPlanes;        // the number of color planes (must be 1)
    uint16_t biBitCount;      // the number of bits per pixel
    uint32_t biCompression;   // the compression method being used
    uint32_t biSizeImage;     // the size of the raw bitmap data (in bytes) (can be 0 for compression method BI_RGB)
    int32_t  biXPelsPerMeter; // the horizontal resolution of the image in pixels per meter
    int32_t  biYPelsPerMeter; // the vertical resolution of the image in pixels per meter
    uint32_t biClrUsed;       // the number of colors in the color palette (can be 0 to indicate the maximum number of colors with regard to the number of bits per pixel should be used)
    uint32_t biClrImportant;  // the number of important colors used (can be 0 to indicate every color is important)
} BITMAPINFOHEADER; // ( == BITMAPV1HEADER)

// bitmap type 'Windows Bitmap' 
#define BF_TYPE_BM (('B' << 0) | ('M' << 8))

// bitmap data compression method: no compression
#define BI_RGB (0)

// default values for the application reserved fields in the BITMAPFILEHEADER struct
#define DEFAULT_APP_BMP_RESERVED1 (('J' << 0) | ('N' << 8))
#define DEFAULT_APP_BMP_RESERVED0 (('B' << 0) | ('M' << 8))

// default value for bitmap/screen resolution
#define DEFAULT_DPI (72.0)

// inches per meter
#define INCHES_PER_METER (39.3701)

int main(int argc, char *argv[])
{
    init(argc, argv);

    file_buffer rbuf, gbuf, bbuf, outbuf;
    int         rfd,  gfd,  bfd,  outfd;

    if ((rfd = open(red_input_file, O_RDONLY)) == -1)
    {
        printf("%s\n", red_input_file);

        perror("Could not open input file for the red channel");
        exit(EXIT_FAILURE);
    }

    if ((gfd = open(green_input_file, O_RDONLY)) == -1)
    {
        // clean up upon error
        close(rfd);

        perror("Could not open input file for the green channel");
        exit(EXIT_FAILURE);
    }

    if ((bfd = open(blue_input_file, O_RDONLY)) == -1)
    {
        // clean up upon error
        close(rfd);
        close(bfd);

        perror("Could not open input file for the blue channel");
        exit(EXIT_FAILURE);
    }

    if (output_file != NULL)
    {
        if ((outfd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH)) == -1)
        {
            // clean up upon error
            close(rfd);
            close(bfd);
            close(bfd);

            perror("Could not create output file");
            exit(EXIT_FAILURE);
        }
    }
    else
        outfd = STDOUT_FILENO;

    file_buffer_clear(&rbuf,   rfd);
    file_buffer_clear(&gbuf,   gfd);
    file_buffer_clear(&bbuf,   bfd);
    file_buffer_clear(&outbuf, outfd);

    // a bmp file needs row alignment on a 4-byte mark, so we calculating a possibily needed offset
    uint32_t unalignedWidth = sizeof(color_bgr_t)  * width,
             alignedWidth   = (((8 * unalignedWidth) + 31) / 32) * 4,
             rowOffset      = alignedWidth - unalignedWidth;

    // BMP file header
    BITMAPFILEHEADER bfHeader =
    {
        .bfType      = BF_TYPE_BM,
        .bfSize      = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + (alignedWidth * height),
        .bfReserved1 = DEFAULT_APP_BMP_RESERVED1,
        .bfReserved0 = DEFAULT_APP_BMP_RESERVED0,
        .bfOffBits   = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER)
    };

    // BMP info header
    BITMAPINFOHEADER biHeader =
    {
        .biSize          = sizeof(BITMAPINFOHEADER),
        .biWidth         = width,
        .biHeight        = height,
        .biPlanes        = 1,
        .biBitCount      = 24,
        .biCompression   = BI_RGB,
        .biSizeImage     = 0,
        .biXPelsPerMeter = (int32_t)(DEFAULT_DPI * INCHES_PER_METER),
        .biYPelsPerMeter = (int32_t)(DEFAULT_DPI * INCHES_PER_METER),
        .biClrUsed       = 0,
        .biClrImportant  = 0
    };

    // write the bmp headers
    file_buffer_write(&outbuf, &bfHeader, sizeof(BITMAPFILEHEADER));
    file_buffer_write(&outbuf, &biHeader, sizeof(BITMAPINFOHEADER));

    // helper memory location filled with zeros
    uint8_t empty[] = { 0, 0, 0 };

    // in memory color representation to temporaly store read color data
    color_bgr_t col;

    // row major traversing the bitmap data
    for (long y = 0; y < height; y++)
    {
        for (long x = 0; x < width; x++)
        {
            // read the channel data from the input buffers
            file_buffer_read (&bbuf, &(col.b), sizeof(uint8_t));
            file_buffer_read (&gbuf, &(col.g), sizeof(uint8_t));
            file_buffer_read (&rbuf, &(col.r), sizeof(uint8_t));

            // write the composed pixel to the bmp image data into the output buffer
            file_buffer_write(&outbuf, &col, sizeof(color_bgr_t));
        }

        // adjust to bmp image row's 4-byte alignment
        file_buffer_write(&outbuf, &empty, rowOffset);
    }

    file_buffer_flush(&outbuf);

    close(rfd);
    close(gfd);
    close(bfd);
    close(outfd);
}