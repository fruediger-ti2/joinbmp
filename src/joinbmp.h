/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 7 - Task 4                                                        *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     joinbmp.h                                        *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/joinbmp         *
 *                                                                            *
 ******************************************************************************/

#ifndef JOINBMP_H__
#define JOINBMP_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <libgen.h>
#include <getopt.h>
    
#define VERSION_MAJOR (0)
#define VERSION_MINOR (1)
#define VERSION_PATCH (0)

uint32_t  width            = 0;
uint32_t  height           = 0;
char     *red_input_file   = NULL;
char     *green_input_file = NULL;
char     *blue_input_file  = NULL;
char     *output_file      = NULL;

void init(int argc, char *argv[]);

static const struct option options[] =
{
    { "width",   required_argument, NULL, 'w' },
    { "height",  required_argument, NULL, 'h' },
    { "red",     required_argument, NULL, 'r' },
    { "green",   required_argument, NULL, 'g' },
    { "blue",    required_argument, NULL, 'b' },
    { "output",  required_argument, NULL, 'o' },
    { "version", no_argument,       NULL, 'v' },
    { "help",    no_argument,       NULL, 'H' }
};

static void version()
{
    printf("%i.%i.%i\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
}

static void usage(const char *executableName)
{
    printf("USAGE: %s [OPTION...] [WIDTH] [HEIGHT] [RED-CHANNEL-INPUTFILE] [GREEN-CHANNEL-INPUTFILE] [BLUE-CHANNEL-INPUT] [OUTPUTFILE]\n", executableName);
    printf("\n");
    printf("OPTIONs are:\n");
    printf("\n");
    printf("    -w=, --width=     \n");
    printf("\n");
    printf("    -h=, --height=    \n");
    printf("\n");
    printf("    -r=, --red=       \n");
    printf("\n");
    printf("    -g=, --green=     \n");
    printf("\n");
    printf("    -b=, --blue=      \n");
    printf("\n");
    printf("    -o=, --output=    \n");
    printf("\n");
    printf("    -v, --version     \n");
    printf("\n");
    printf("    --help            \n");
}

void init(int argc, char *argv[])
{
    int showVersion = 0, showHelp = 0;

    int optidx = 0, opt;
    while ((opt = getopt_long(argc, argv, "w:h:r:g:b:o:v", options, &optidx)) != -1)
        switch (opt)
        {
            case 'w':
                width = atoll(optarg);
                break;

            case 'h':
                height = atoll(optarg);
                break;

            case 'r':
                red_input_file = optarg;
                break;

            case 'g':
                green_input_file = optarg;
                break;

            case 'b':
                blue_input_file = optarg;
                break;

            case 'o':
                output_file = optarg;
                break;

            case 'v':
                showVersion = 1;
                break;

            case 'H':
                showHelp = 1;
                break;

            case ':':
            case '?':
            default:
                printf("Try this instead:\n");
                usage(basename(argv[0]));
                exit(EXIT_FAILURE);
                break;
        }

    if (showHelp)
    {
        usage(basename(argv[0]));
        exit(EXIT_SUCCESS);
    }

    if (showVersion)
    {
        version();
        exit(EXIT_SUCCESS);
    }

    optidx = optind;

    if (width == 0 && (optidx >= argc || (width = atoll(argv[optidx++])) == 0))
    {
        printf("Either WIDTH was not set or it was set to 0.\n");
        printf("Try this instead:\n");
        usage(basename(argv[0]));
        exit(EXIT_FAILURE);
    }

    if (height == 0 && (optidx >= argc || (height = atoll(argv[optidx++])) == 0))
    {
        printf("Either HEIGHT was not set or it was set to 0.\n");
        printf("Try this instead:\n");
        usage(basename(argv[0]));
        exit(EXIT_FAILURE);
    }

    if (red_input_file == NULL)
    {
        if (optidx < argc)
            red_input_file = argv[optidx++];
        else
        {
            printf("No input file for the RED channel was given.\n");
            printf("Try this instead:\n");
            usage(basename(argv[0]));
            exit(EXIT_FAILURE);
        }
    }

    if (green_input_file == NULL)
    {
        if (optidx < argc)
            green_input_file = argv[optidx++];
        else
        {
            printf("No input file for the GREEN channel was given.\n");
            printf("Try this instead:\n");
            usage(basename(argv[0]));
            exit(EXIT_FAILURE);
        }
    }

    if (blue_input_file == NULL)
    {
        if (optidx < argc)
            blue_input_file = argv[optidx++];
        else
        {
            printf("No input file for the BLUE channel was given.\n");
            printf("Try this instead:\n");
            usage(basename(argv[0]));
            exit(EXIT_FAILURE);
        }
    }

    if (output_file == NULL && optidx < argc)
    {
        output_file = argv[optidx];
    }
}

#endif